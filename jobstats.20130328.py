import numpy as np
import pylab as plt
import sys

def str2secs(time):
	"""
	Convert a string of the form HH:MM:SS into a duration in seconds
	"""
        H, M, S = time.split(':')
        return 3600.0*float(H) + 60.0*float(M) + float(S)

def main():
    """
    this is the main routine.
    """
    if len(sys.argv) < 2:
	    sys.exit('Usage: ' + sys.argv[0] + ' [Accounting files]')
    else:
	    joblist = sys.argv[1:]


    jobs = alljobs(joblist)
    userEffList = list()
    users = {}
    #Add each job to the users dictionary
    for job in jobs:
	    if job.user not in users:
		    users[job.user] = user(job)
	    else:
		    users[job.user].joblist.append(job)
    #Build a list of user statistics

    for usr in users:
	    userEffList.append([users[usr].sortMetric(), \
				users[usr].avgEfficiency(), \
				users[usr].avgMemFraction(), \
				users[usr].ttlWalltime()/3600.0, \
				users[usr].name])

    userEffList.sort(reverse=True)
    #Print User statistics
    for usr in userEffList:
	    print(usr[1], usr[2], usr[3], usr[4])
    #Print detailed stats for "top 10" users in sorted list
    for usr in userEffList[:10]:
	    users[usr[4]].printStats()
    #Make plots
    makePlots('fullList', jobs, scatterPlots=True)


    
def alljobs(files):
    """
    routine to read the accounting logs returning a dictionary of jobs
    """
    alljobs = list()
    for file in files:
        try:
		f = open(file, 'r').readlines()
	except IOError:
		sys.exit('IO Error: File ' + file + ' not found')
        for rown in f:
	    row = rown.split()
	    cores = 1
	    #Exctract the number of cores from the accounting files
	    if any('resources_used' in s for s in row):
	        id = row[1].split(';')[2].split('.')[0]
		for col in row:
		    if 'user=' in col:
			    user = col.split('=')[-1]
		    elif 'queue=' in col:
			    queue = col.split('=')[-1]
		    elif 'cput=' in col:
		        cput = col.split('=')[-1]
		    elif 'used.mem=' in col:
		        mem = col.split('=')[-1]
		    elif 'used.vmem=' in col:
		        vmem = col.split('=')[-1]
		    elif 'resources_used.walltime=' in col:
		        walltime = col.split('=')[-1]
		    elif 'Resource_List.procs=' in col:
		        cores = col.split('=')[-1]
		    elif 'Exit_status=' in col:
		        exitcode = col.split('=')[-1]
		    elif 'Resource_List.nodes=' in col and '-' not in col:
		        col2 = col.split(':')
			if len(col2) > 1:
			    cores = int(col2[0].split('=')[-1])* \
				    int(col2[1].split('=')[-1])
			else:
			    cores = col2[0].split('=')[-1]
                alljobs.append(job(id, user, queue, cput, mem, vmem, \
				   walltime,cores,exitcode))
    return alljobs

def makePlots(filename, jobs, scatterPlots=False):
	efficiencies = list()
	memUnused = list()
	cores = list()
	walltime = list()
	coreHours = list()
	efficienciesCH = list()
	if scatterPlots:
		memUnused2d = list()
		cores2d = list()
		walltime2d = list()
		coreHours2d = list()
	for job in jobs:
		memUnused.append(job.memUnusedFraction()*100.0)
		cores.append(job.cores)
		coreHours.append(job.walltime/3600.0*job.cores)
		walltime.append(job.walltime/3600.0)
		if 0.0 < job.efficiency() < 2.0:
			efficiencies.append(job.efficiency()*100.0)
			if scatterPlots:
				memUnused2d.append(memUnused[-1])
				if job.walltime/3600.0 < 400.0:
				    coreHours2d.append(coreHours[-1])
				    efficienciesCH.append(efficiencies[-1])

	if scatterPlots:
	    plt.clf()
            plt.hexbin(efficiencies, memUnused2d, bins='log', gridsize=1000)
	    plt.xlabel('Efficiency (%)')
	    plt.xlim(0.0, 110.0)
	    plt.ylabel('Unused Memory (%)')
	    plt.ylim(0.0, 100.0)
	    plt.savefig(filename + '.memVsE.png')
	    plt.clf()
	    plt.hexbin(efficienciesCH, coreHours2d, bins='log', \
		       gridsize=(200, 2000))
	    plt.xlabel('Efficiency (%)')
	    plt.xlim(0.0, 110.0)
	    plt.ylabel('Walltime x Cores (core hours)')
	    plt.ylim(0.0, 400.0)
	    plt.savefig(filename + '.coreHoursVsE.png')
	
	plt.clf()
	plt.cla()
	plt.hist(efficiencies, bins=1000, log=True, color='k')
	plt.xlabel('Efficiencies (%)')
	plt.xlim(0.0, 150.0)
	plt.savefig(filename + '.efficiencies.png')
	plt.cla()
	plt.hist(memUnused, bins=1000, log=True, color='k')
	plt.xlabel('Unused Memory (%)')
	plt.xlim(0.0, 110.0)
	plt.savefig(filename + '.memUnused.png')
	plt.cla()
	plt.hist(cores, bins=max(cores), log=True, color='k')
	plt.xlabel('Number of cores')
	plt.xlim(0, 100)
	plt.savefig(filename + '.cores.png')
	plt.cla()
	plt.hist(walltime, bins=1000, log=True, color='k')
	plt.xlim(0, 240.0)
	plt.xlabel('Walltime (hours)')
	plt.savefig(filename + '.walltime.png')
	plt.cla()
	plt.hist(coreHours, bins=1000, log=True, color='k')
	plt.xlim(0, 1000.0)
	plt.xlabel('Walltime x Cores (core hours)')
	plt.savefig(filename + '.corehours.png')
	return
	

class user():
	"""
	A class to hold user information
	"""
	def __init__(self, job):
		self.name = job.user
		self.joblist = list()
		self.joblist.append(job)

	def avgEfficiency(self):
		"""Average efficiency of user's jobs"""
		numJobs = len(self.joblist)
		sumEfficiencies = 0.0
		for job in self.joblist:
			sumEfficiencies += job.efficiency()
		return sumEfficiencies / float(numJobs)
	def avgMem(self):
		"""Average memory of user's jobs"""
		numJobs = len(self.joblist)
		sumMem = 0.0
		for job in self.joblist:
			sumMem += job.mem/job.cores
		return sumMem / float(numJobs)
	def avgMemFraction(self):
		"""Average memory use fraction"""
		sumMem = 0.0
		for job in self.joblist:
			sumMem += 1.0 - job.memUnusedFraction()
		return sumMem / float(len(self.joblist))
	def ttlWalltime(self):
		"""Total walltime*cores"""
		ttlwt = 0.0
		for job in self.joblist:
			ttlwt += job.walltime*float(job.cores)
		return ttlwt
	def sortMetric(self):
		"""Metric used for sorting users"""
		metric = 0.0
		for job in self.joblist:
		    if 0.0 < job.efficiency() < 1.0:	
			x = job.efficiency()/(1.0-job.efficiency())
		    else:
		        x = 1.0e24
		    metric += np.exp(-x)*job.cores*job.walltime\
				  *job.memUnusedFraction()
		metric *= self.ttlWalltime()/float(len(self.joblist))
		return metric 
	
	def addJob(self, job):
		self.joblist.append(job)

	def printJobs(self, nJobs):
		"""Print the last nJobs jobs added to user's joblist"""
		for job in self.joblist[-1*nJobs:]:
			print(job.id)

	def printAllJobs(self):
		for job in self.joblist:
			job.printStats()

	def badExitJobs(self):
		badExits = list()
		for job in self.joblist:
			if job.exitcode != 0:
				badExits.append(job)
		return badExits
	def superEfficientJobs(self):
		effJobs = list()
		for job in self.joblist:
			if job.isSuperEfficient():
				effJobs.append(job)
		return effJobs

	def printStats(self):
		"""Print detailed human readable statistics for user"""
		#nq is a dictionary mapping queues used to how many jobs
		nq = {}
		#avgCores is the average number of cores used
		avgCores = 0.0
		for job in self.joblist:
			avgCores += job.cores
			if job.queue in nq:
				nq[job.queue] += 1
			else:
				nq[job.queue] = 1
		avgCores /= len(self.joblist)
		print('******************************')
		print('User: ' + self.name)
		print('Number of jobs: ' + str(len(self.joblist)))
		print('Average Walltime: ' + \
		      str(self.ttlWalltime()/len(self.joblist)/3600.0) \
		      + ' core hours')
		print('Total Walltime: ' + \
		      str(self.ttlWalltime()/3600.0) + ' core hours')
		print('Average efficiency: ' + \
		      str(self.avgEfficiency()*100.0)\
		      + '%')
		print('Average Number of Cores: ' + \
		      str(avgCores))
		print('Average Memory per core: ' + \
		      str(self.avgMem()/1048576.0) \
		      + ' GB')
		print('Queues used (number of jobs):')
		for q in nq:
			print(str(q) + ' (' +  str(nq[q]) + ')')
		print('Some jobs: ')
		self.printJobs(5)
		print('Jobs with Bad Exits (' + \
		      str(len(self.badExitJobs())) + '): ')
		for job in self.badExitJobs()[:10]:
			print(str(job.id) + ' exit code: ' \
			      + str(job.exitcode))
		print('Super-efficient jobs (' + \
		      str(len(self.superEfficientJobs())) + '): ')
		for job in self.superEfficientJobs()[:10]:
			print(str(job.id) + ' efficiency: ' \
			      + str(job.efficiency()))
		print('******************************')
		

class job():
    """
    A class to hold PBS job statistics
    """
    def __init__(self, id, user, queue, cput, mem, \
		 vmem, walltime, cores, exitcode):
        #we read everything in as strings (dtype='str'), cast them to floats
        try:
            self.id = int(id)
        except(ValueError):
            self.id = np.nan
        self.user = user
        self.queue = queue
        try:
            self.cput = str2secs(cput)
        except(ValueError):
            self.cput = np.nan
        try:
            self.mem = float(mem[0:-2])
        except(ValueError):
            self.mem = np.nan
        try:
            self.vmem = float(vmem[0:-2])
        except(ValueError):
            self.vmem = np.nan
        try:
            self.walltime = str2secs(walltime)
	    self.walltime = max(0.1, self.walltime)
        except(ValueError):
            self.walltime = np.nan
	try:
	    self.cores = int(cores)
	except(ValueError):
	    self.cores = 1
	try:
	    self.exitcode = int(exitcode)
	except(ValueError):
	    self.exitcode = -100
	self.queue = queue
    def efficiency(self):
	    """The CPU usage efficiency of the job"""
	    return self.cput / (self.walltime * float(self.cores))
    def memUnused(self):
	    """ Unused memory in GB """
	    memUsed = self.mem / 1048576.0
	    memPerCore = {'hb':1.7, 'lm':5.7, 'scalemp':8.0, \
			  'lmgpu':5.7, 'sw':2.7}
	    if self.queue in memPerCore:
		    memTotal = memPerCore[self.queue]*self.cores
	    else:
		    memTotal = 3.0*self.cores
	    return (memTotal - memUsed)
    def memUnusedFraction(self):
	    memPerCore = {'hb':1.7, 'lm':5.7, 'scalemp':8.0, \
			  'lmgpu':5.7, 'sw':2.7}
	    if self.queue in memPerCore:
		    return self.memUnused()/(memPerCore[self.queue]*self.cores)
	    else:
		    return self.memUnused()/(3.0*self.cores)
    
    def isSuperEfficient(self):
	    """Returns true if the job has a strange efficiency result"""
	    prob = False
	    if self.cput < 0:
		    prob = True
	    elif self.cput > self.walltime*self.cores:
		    prob = True
	    return prob
    def printStats(self):
	    """Print human readable stats for the job"""
	    print('******************************')
	    print('Job: ' + str(self.id))
	    print('User: ' + str(self.user))
	    print('queue: ' + str(self.queue))
	    print('cores: ' + str(self.cores))
	    print('walltime: ' + str(self.walltime/3600.0) + ' hours')
	    print('cpu time: ' + str(self.cput/3600.0) + ' hours')
	    print('efficiency: ' + str(self.efficiency() * 100.0) + ' %') 
	    print('mem: ' + str(self.mem / 1048576.0) + ' GB')
	    print('vmem: ' + str(self.vmem / 1048576.0) + ' GB')
	    print('exitcode: ' + str(self.exitcode) )
	    print('******************************')
	    return
	
	    

if __name__ == '__main__':
    main()
