#!/software/tools/python-2.6.7/bin/python
import jobstats
import sys

def main():
    """
    Main routine: Print User stats
    """
    if len(sys.argv) < 2:
        sys.exit('Usage: ' + sys.argv[0] + ' username ' \
                 + ' [Accounting files]')
    else:
        joblist = sys.argv[1:]

    list = None
    jobs = jobstats.alljobs(joblist)
    for job in jobs:
        if 'bschenke' in job.user and job.exitcode == -11:
            if list == None:
                list = jobstats.jobGroup(job)
            else:
                list.addJob(job)
    list.printStats()
    list.printAllJobs()
    nodes={}
    for j in list.joblist:
        for n in j.nodes:
            if n in nodes:
                nodes[n] += 1
            else:
                nodes[n] = 1
    print(nodes)
    return

if __name__ == '__main__':
    main()

            
    
